import hashlib
import locale
from pathlib import Path

import minicli
import mistune
from jinja2 import Environment as Env
from jinja2 import FileSystemLoader
from slugify import slugify

HERE = Path(".").resolve()
PUBLIC = HERE / "public"
# Utile pour le rendu des langues en français.
locale.setlocale(locale.LC_ALL, "fr_FR.UTF-8")

# Pour que Jinja2 sache où trouver les fichiers de templates.
environment = Env(loader=FileSystemLoader(str(HERE / "templates")))

SPECIAL_COMMENTS = {
    "Préambule": {"class": "notice", "open": True, "id": "preambule"},
    "Colophon": {"class": "notice", "open": True, "id": "colophon"},
    "Question": {"class": "question", "open": True},
    "Documentation": {"class": "documentation", "open": False},
}


class TitlesAnchorsRenderer(mistune.HTMLRenderer):
    """Custom renderer for titles with anchors."""

    def heading(self, text, level):
        slug = slugify(text)
        return (
            f'<h{level} id="{slug}">'
            f'<a href="#{slug}" title="Ancre vers cette partie">⚓︎</a> '
            f"{text}"
            f"</h{level}>"
        )


class CommentToDetailsRenderer(mistune.HTMLRenderer):
    """Custom renderer to convert HTML comments to details/summary."""

    def block_html(self, html: str) -> str:
        if "<!--" in html:
            comment_content = html.strip()[len("<!--") : -len("-->")]
            default_title = "Commentaires"
            comment_meta = {}
            if comment_content.strip().startswith(tuple(SPECIAL_COMMENTS.keys())):
                comment_title, comment_content = comment_content.split("\n", 1)
                comment_meta = SPECIAL_COMMENTS[comment_title.strip()]
            else:
                comment_title = default_title
            # Create a unique id for the internal link.
            comment_id = (
                comment_meta.get("id")
                or hashlib.sha256(comment_content.encode("utf-8")).hexdigest()
            )
            # Convert the content of the comment if it contains markdown.
            comment_content = md(comment_content)
            comment_class = comment_meta.get("class", "")
            comment_open = comment_meta.get("open", False)
            html = f"""

            <details class="{comment_class}" {"open" if comment_open else ""}>
            <summary id="comment-{comment_id}">
                <a href="#comment-{comment_id}"
                    onclick="this.parentElement.click()">⚓︎</a>
                {comment_title}
            </summary>

            {comment_content}

            </details>

            """
        return super().block_html(html)


class CustomRenderer(
    TitlesAnchorsRenderer, CommentToDetailsRenderer, mistune.HTMLRenderer
):
    ...


# Le convertisseur markdown avec l’activation des plugins utiles.
md = mistune.create_markdown(
    plugins=["strikethrough", "table", "url", "task_lists", "mark"],
    escape=False,
    renderer=CustomRenderer(escape=False),
)


@minicli.cli
def build():
    template = environment.get_template("base.html")
    markdown = (HERE / "statuts.md").read_text()
    titre, markdown = markdown.split("\n", 1)
    titre = titre[2:]
    statuts = md(markdown)
    content = template.render(titre=titre, statuts=statuts)
    (PUBLIC / "index.html").write_text(content)


if __name__ == "__main__":
    minicli.run()
